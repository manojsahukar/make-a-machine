# Make-A-Machine

This is the official repository of *Make-A-Machine* group project for [Fab Academy 2019](http://fabacademy.org/2019/) at [Vigyan Ashram](http://vigyanashram.com/).  
The team is building a machine to automate small scale agriculture.

### Similar Open Source projects
- [FarmBot](https://en.wikipedia.org/wiki/FarmBot)

## Fab Academy 2019

The course schedule is available [here](http://fabacademy.org/2019/schedule.html).

## Fablab Vigyan Ashram
The official fablab page for Fab Academy 2019 is [here](http://fabacademy.org/2019/labs/vigyanashram/)

## Scrum Roles

### Product Owner
- [Vigyan Ashram](http://vigyanashram.com/)

### Scrum Master
- [Manoj Kumar Sahukar](http://fabacademy.org/2019/labs/vigyanashram/students/manoj-sahukar)  

### Development Team
- [Aaditi Prakash Kharade](http://fabacademy.org/2019/labs/vigyanashram/students/aaditi-kharade)  
- [Anand Kishor Verma](http://fabacademy.org/2019/labs/vigyanashram/students/anand-verma)  
- [Hemang Vellore](http://fabacademy.org/2019/labs/vigyanashram/students/hemang-vellore)  
- [Jayadip Babanrao Sarode](http://fabacademy.org/2019/labs/vigyanashram/students/jayadip-sarode)  
- [Manoj Kumar Sahukar](http://fabacademy.org/2019/labs/vigyanashram/students/manoj-sahukar)  
- [Pooja Gajanan Jadhav](http://fabacademy.org/2019/labs/vigyanashram/students/pooja-jadhav)  
- [Tushar Amar Kukreja](http://fabacademy.org/2019/labs/vigyanashram/students/tushar-kukreja)  
- [Vaibhav Saxena](http://fabacademy.org/2019/labs/vigyanashram/students/vaibhav-saxena)  

## Live Documetation

Check repository **Wiki** for live documentation. Access [here](https://gitlab.com/manojsahukar/make-a-machine/wikis/home).

## Project Timeline

Daily Scrum. Access [here](https://manojsahukar.gitlab.io/make-a-machine/).

